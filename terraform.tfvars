# What's included:
# New VPC
# EC2 instance with Ubuntu and apache2

# Whats need to be set in this project, file(terraform.tfvars):
aws_region = "eu-central-1"
aws_zone = "eu-central-1a"
project_short_name = "project1"

# this is learn project so lot's to set up in main.tf


# 1. Create vpc
# 2. Create Internet Gateway (to send trafic out)
# 3. Create Custom Route Table
# 4. Create Subnet 
# 5. Associate subnet with Route Table
# 6. Create Security Group to allow port 22,80,443
# 7. Create a network interface with an ip in the subnet that was created in step 4
# 8. Assign an elastic IP to the network interface created in step 7
# 9. Create key-value pair for port 22
# 10. Create Ubuntu server and install/enable apache2